---
---

# Pango

Pango is a library for laying out and rendering of text, with an emphasis on
internationalization. Pango can be used anywhere that text layout is needed,
though most of the work on Pango so far has been done in the context of the
GTK widget toolkit. Pango forms the core of text and font handling for GTK.

The name comes from the Greek Παν ("Pan"), meaning "all", and the Japanese
語 ("Go"), meaning "language".

## Features

Pango is designed to be modular; the core Pango layout engine can be used
with different font backends. There are three basic backends, with multiple
options for rendering with each:

- Client side fonts using the FreeType and fontconfig libraries. Rendering
  can be with Cairo or Xft libraries, or directly to an in-memory buffer with
  no additional libraries.
- Native fonts on Microsoft Windows. Rendering can be done via Cairo or
  directly using the native Win32 API.
- Native fonts on MacOS X, rendering via Cairo.

As well as the low-level layout rendering routines, Pango includes
`PangoLayout`, a high-level driver for laying out entire blocks of text, and
routines to assist in editing internationalized text.

Complex-text support is provided by [HarfBuzz](https://harfbuzz.org/), on all
platforms.

The integration of Pango with [Cairo](https://www.cairographics.org) provides
a complete solution with high quality text handling and graphics rendering.

## Documentation

- [API reference for Pango](https://docs.gtk.org/Pango/)
- [API reference for Cairo integration](https://docs.gtk.org/PangoCairo/)
- [API reference for FreeType2 integration](https://docs.gtk.org/PangoFT2/)
- [API reference for Fontconfig integration](https://docs.gtk.org/PangoFc/)
- [Pango markup documentation](https://docs.gtk.org/Pango/pango_markup.html)

## Sources

The Pango source is available on GNOME's GitLab instance at:
https://gitlab.gnome.org/GNOME/pango

You can download the release archives from:
https://download.gnome.org/sources/pango

## Reporting issues

If you have an issue using Pango, open a topic on GNOME's Discourse instance
under the ["pango" tag](https://discourse.gnome.org/tag/pango).

You can report bugs on the project's [GitLab issue
tracker](https://gitlab.gnome.org/GNOME/pango/-/issues).
